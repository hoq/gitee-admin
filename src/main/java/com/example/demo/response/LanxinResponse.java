package com.example.demo.response;

/**
 * @ClassName LanxinResponse
 * @Description
 * @Author za-zhouyunxing
 * @Date 2019/5/22 16:31
 * @Version 1.0
 */
public class LanxinResponse {
    private Integer errcode;

    private String errmsg;

    public LanxinResponse() {
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    @Override
    public String toString() {
        return "LanxinResponse{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                '}';
    }
}
