package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.bean.OrgMember;

@Repository
@Mapper
public interface OrgMemberDao{
    List<OrgMember> getMemberById(int id);

    List<OrgMember> selectAll();
}
