package com.example.demo.dao;

import com.example.demo.bean.OrgStruct;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OrgStructDao {
    List<OrgStruct> getStructById(int id);

    List<OrgStruct> selectAll();
}
