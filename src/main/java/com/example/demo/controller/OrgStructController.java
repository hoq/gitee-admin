package com.example.demo.controller;

import com.example.demo.bean.OrgMember;
import com.example.demo.bean.OrgStruct;
import com.example.demo.service.impl.OrgStructServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/struct")
public class OrgStructController {
    @Autowired
    private OrgStructServiceImpl lanxinService;

    @RequestMapping("/id")
    public List<OrgStruct> struct(int id) {
        List<OrgStruct> list = lanxinService.getStructById(id);
        return list;
    }

    @RequestMapping("/all")
    public List<OrgStruct> structAll() {
        List<OrgStruct> list = lanxinService.selectAll();
        return list;
    }
}
