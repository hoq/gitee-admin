package com.example.demo.controller;

import com.example.demo.bean.OrgMember;
import com.example.demo.service.impl.OrgMemberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/member")
public class OrgMemberController {
    @Autowired
    private OrgMemberServiceImpl lanxinService;

    @RequestMapping("/id")
    public List<OrgMember> member(int id) {
        List<OrgMember> list = lanxinService.getMemberById(id);
        return list;
    }

    @RequestMapping("/all")
    public List<OrgMember> memberAll() {
        List<OrgMember> list = lanxinService.selectAll();
        return list;
    }
}
