package com.example.demo.utils;

public class LanXinUtils {
    public static final String appid = "100675";
    public static final String secret = "SnE_@X9f@id8T0OUUC";

    //获取access_token
    public static final String getTokenURL = "http://comac.e.lanxin.cn/cgi-bin/token";

    //发送客服消息
    public static final String getCustomURL = "https://comac.e.lanxin.cn/cgi-bin/message/custom/send";

    //查询组织成员数据
    public static final String getOrgURL = "http://comac.e.lanxin.cn/cgi-bin/member/get";

    //查询组织成员数据
    public static final String getOrgResult = "http://comac.e.lanxin.cn/cgi-bin/org/struct/parent/get";
}
