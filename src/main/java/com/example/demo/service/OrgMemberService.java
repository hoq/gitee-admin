package com.example.demo.service;

import com.example.demo.bean.OrgMember;

import java.util.List;

public interface OrgMemberService {
    List<OrgMember> getMemberById(int id);

    List<OrgMember> selectAll();
}
