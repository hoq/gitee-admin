package com.example.demo.service;

import com.example.demo.bean.OrgStruct;

import java.util.List;

public interface OrgStructService {
    List<OrgStruct> getStructById(int id);

    List<OrgStruct> selectAll();
}
