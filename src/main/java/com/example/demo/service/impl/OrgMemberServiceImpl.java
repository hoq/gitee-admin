package com.example.demo.service.impl;

import com.example.demo.bean.OrgMember;
import com.example.demo.dao.OrgMemberDao;
import com.example.demo.service.OrgMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrgMemberServiceImpl implements OrgMemberService {

    @Autowired
    private OrgMemberDao orgMemberDao;

    @Override
    public List<OrgMember> getMemberById(int id) {
        List<OrgMember> list = orgMemberDao.getMemberById(id);
        return list;
    }

    @Override
    public List<OrgMember> selectAll() {
        List<OrgMember> list = orgMemberDao.selectAll();
        return list;
    }
}
