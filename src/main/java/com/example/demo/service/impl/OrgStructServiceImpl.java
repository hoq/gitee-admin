package com.example.demo.service.impl;

import com.example.demo.bean.OrgStruct;
import com.example.demo.dao.OrgStructDao;
import com.example.demo.service.OrgStructService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrgStructServiceImpl implements OrgStructService {

    @Autowired
    private OrgStructDao orgStructDao;

    @Override
    public List<OrgStruct> getStructById(int id) {
        List<OrgStruct> list = orgStructDao.getStructById(id);
        return list;
    }

    @Override
    public List<OrgStruct> selectAll() {
        List<OrgStruct> list = orgStructDao.selectAll();
        return list;
    }
}
