package com.example.demo.request;

/**
 * @ClassName GetCustomRequest
 * @Description
 * @Author za-zhouyunxing
 * @Date 2019/5/22 16:12
 * @Version 1.0
 */
public class ContentRequest {
    String content;

    public ContentRequest() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
