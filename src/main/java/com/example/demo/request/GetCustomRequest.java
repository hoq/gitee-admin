package com.example.demo.request;

/**
 * @ClassName GetCustomRequest
 * @Description
 * @Author za-zhouyunxing
 * @Date 2019/5/22 16:12
 * @Version 1.0
 */
public class GetCustomRequest {
    String touser;

    String msgtype;

    ContentRequest text;

    public GetCustomRequest() {
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public ContentRequest getText() {
        return text;
    }

    public void setText(ContentRequest text) {
        this.text = text;
    }
}
